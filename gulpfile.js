// config
// use default scss
var src = './src/';
var dist = './dist/';
var npm = './node_modules/';
var browsers = ['> 3%', 'last 2 versions', 'ie 9', 'ios 6', 'android 4'];
// for pug
var pathsView = [
    src + "index.pug"
];

// for js
var appPlugins = [
    npm + 'jquery/dist/jquery.js',
    npm + 'vue/dist/vue.js'
];

var appScripts = [
    src + "scripts/app.js",
    src + "scripts/**/*.js"
];

// json
var pathData = [
    "src/*.json"
];


// modules mpn
var gulp = require('gulp'),
    pug = require("gulp-pug"),
    plumber = require('gulp-plumber'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    stylus = require("gulp-stylus"),
    rupture = require("rupture"),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;


// taks pug
var taskPugSelf = function () {
    return gulp.src(pathsView)
        .pipe(plumber())
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest(dist));
};
gulp.task("view", function () {
    return taskPugSelf();
});

// taks styles for stylus
var taskStylusSelf = function () {
    return gulp.src(src + 'styles/index.styl')
        .pipe(stylus({
            use:[rupture()]
        }))
        .pipe(gulp.dest(dist + "styles/"));
};
gulp.task("styles", function () {
    return taskStylusSelf();
});

gulp.task("data", function () {
    return gulp.src(pathData)
        .pipe(gulp.dest(dist));
});

// New taks advanced

// sass mappgins files
gulp.task('sass:dev', function () {
    var processors = [
        autoprefixer({browsers: browsers})
    ];

    gulp.src(src + 'scss/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dist + 'styles/'))
    .pipe(browserSync.stream());
});

// sass dist remove source maps
gulp.task('sass:dist', function () {
    var processors = [
        autoprefixer({browsers: browsers}),
        cssnano()
    ];

    gulp.src(src + 'scss/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(gulp.dest(dist + 'styles/'));
});

// copy images
gulp.task('images', function () {
    gulp.src(src + 'images/**/*(*.png|*.jpg|*.jpeg|*.gif|*.svg)')
    .pipe(gulp.dest(dist + 'images/'));
});

// plugins js minify
gulp.task('pluginsjs', function () {
    gulp.src(appPlugins)
    .pipe(concat('plugins.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dist + 'scripts/'));
});

gulp.task('appjs', function () {
    gulp.src(appScripts)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'))
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(dist + 'scripts/'));
});


// Rerun the task when a file changes
gulp.task('watch', function () {
    gulp.watch(src + 'styles/**/*.styl', ['styles']);
    gulp.watch(src + 'scss/**/*.scss', ['sass:dev']);
    gulp.watch(src + 'scripts/**/*.js', ['appjs']).on('change', reload);
    gulp.watch(src + './**/*.pug', ['view']).on('change', reload);
});

gulp.task("serve", function () {
    browserSync.init({
        reloadDebounce: 500,
        port: 8089,
        server: {
            baseDir: dist
        }
    });
});


// tasks globals
gulp.task('assets', ['view', 'data', 'styles', 'pluginsjs', 'appjs', 'images']);

// build all
gulp.task('build', ['sass:dev', 'assets', 'watch']);
// gulp minify all: gulp dist
gulp.task('dist', ['sass:dist',  'assets']);
// dev default tasks: gulp
gulp.task('default', ['build', 'serve']);
