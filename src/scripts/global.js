/*
  constants and global functions
*/

var JSON_FILE = '/books-schema.json';

/*
 @method loadJSON
 source: https://codepen.io/KryptoniteDove/post/load-json-file-locally-using-pure-javascript
*/

function loadJSON (file, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if(xhr.readyState === 4 && xhr.status === 200){
            callback(xhr.responseText);
        }
    };
    xhr.open('GET', file, true);
    xhr.send();
}

var jsonData;
loadJSON(JSON_FILE, function (response) {
    jsonData = JSON.parse(response);
});



new Vue({
    el: "#app",
    data: {
        search: '',
        autocomplete: false,
        scategory: 'horror',
        data: {
            data: [],
            entities: {
                categories: [],
                lang: [],
                edition: []
            }
        }
    },
    created: function () {
        var _self = this;
        loadJSON(JSON_FILE, function (response) {
            _self.data = JSON.parse(response);
        });
    },
    computed: {
        filteredData: function () {
            var _self = this;
            // Apply filter first
            var result = _self.data.data;
            if (_self.search.length > 2) {
                result = result.filter(function (item) {
                    return item.title.toLowerCase().includes(_self.search);
                });
            }
            return result;
        }
    },
    methods: {
        saveSearch: function () {
            if (!this.search) return;
            var _search = {label: this.search};
            this.data.entities.saved.push(_search);
            this.search = '';
        },
        removeSaved: function (index) {
            console.log(index);
            this.data.entities.saved.splice(index, 1);
        }
    }
});

